class UserStocksController < ApplicationController

  def create
    stock = Stock.check_db(params[:ticker]) 
    if stock.blank?
      stock = Stock.new_lookup(params[:ticker])
      stock.save
    end
    @user_stock = UserStock.new(user:current_user, stock: stock)
    if @user_stock.save
      flash[:notice] = "Stock #{stock.name} succesfully added to portfolio"
    end
    redirect_to my_portfolio_path
  end

  def destroy
    userstock = UserStock.where(stock: params[:id], user: current_user).first
    userstock.destroy
    flash[:notice] = "Stock removed succesfully"
    redirect_to my_portfolio_path
  end

end
