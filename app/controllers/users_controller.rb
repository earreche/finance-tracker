class UsersController < ApplicationController

  def my_portfolio
    @tracked_stocks = current_user.stocks
  end

  def my_friends
    @friends = current_user.friends
  end

  def search
    if params[:email].present?
      @friend = User.where(email: params[:email]).first
      if !@friend
        flash.now[:alert] = "Please enter a proper symbol to search"
      end
    else
      flash.now[:alert] = "Please enter a email to search"
    end
    respond_to do |format|
      format.js { render partial: 'users/result_friendship' }
    end
  end

end
