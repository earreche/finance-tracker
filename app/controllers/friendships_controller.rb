class FriendshipsController < ApplicationController

  def create
    friend = User.find(params[:friend])
    if friend
      @user_stock = Friendship.new(user:current_user, friend: friend)
      if @user_stock.save
        flash[:notice] = "Friend #{friend.email} succesfully added to portfolio"
      end
    else
      flash[:notice] = "Usuario No Encontrado"
    end
    redirect_to my_friends_path
  end

  def destroy
    friendship = Friendship.where(friend: params[:id], user: current_user).first
    friendship.destroy
    flash[:notice] = "Stock removed succesfully"
    redirect_to my_friends_path
  end

end
