class CreateStocks < ActiveRecord::Migration[6.0]
  def change
    create_table :stocks do |t|
      t.string :ticker, null: false
      t.string :name, null: false
      t.decimal :last_price, null: false, default: 0

      t.timestamps
    end
  end
end
